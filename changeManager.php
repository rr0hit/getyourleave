<?
session_start();

if($_SESSION['type'] == 'administrator') {
    if (isset($_POST['manID'])) {
	    require_once("includes/Administrator.php");

	    if (isset($_POST["empID"])) {
	        $admin = new Administrator($_SESSION['empID']);

	        $manID = $_POST['manID'];
	        if($admin->isEmployee($manID)) {
                if ($_POST["empID"] == $_SESSION["empID"]) {
                    echo "Admin's manager is always admin. Sorry!!";
                }
                else if($admin->detectCycle($_POST['empID'], $manID)) {
                    echo "Cycle detected!!";
                }
                else {
	        	    $admin->setManager($_POST['empID'],$manID);
                    echo "ok";
                }
	        }
	        exit;
	    }
    }
    else {
	    require_once("generateHtml.php");
	    generateChangeManagerForm($_GET["empID"]);
    }
}

else
    echo "You are not authorized to view this page!!";

?>
