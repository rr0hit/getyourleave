<?
session_start();
if($_SESSION['type'] == 'administrator') {
    if (isset($_POST['holidayDate'])) {
        require_once("includes/Administrator.php");
        $empID = $_SESSION["empID"];
        $date = $_POST["holidayDate"];
        $name = $_POST["nameOfHoliday"];
        $admin = new Administrator($empID);
        $admin->addAHoliday($date, $name);
        echo "ok";
    }

    else {
        require_once("generateHtml.php");
        generateAddHolidayForm();
    }
}

else
    echo "Error: Not authorized!!";
?>
