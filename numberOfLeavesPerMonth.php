<?
session_start();
if($_SESSION['type'] == 'administrator') {
    if (isset($_POST['leavesPerMonth'])) {
        require_once("includes/Administrator.php");
        $empID = $_SESSION["empID"];
        $leavesPerMonth = $_POST['leavesPerMonth'];
        if(is_numeric($leavesPerMonth)) {
            $admin = new Administrator($empID);
            $admin->setLeavesPerMonth($leavesPerMonth);
            echo "ok";
        }
        else {
            echo "Error: Input must be a positive integer.";
        }
    }

    else {
        require_once("generateHtml.php");
        require_once("includes/Administrator.php");
        session_start();
        $empID = $_SESSION["empID"];
        $leavesPerMonth = $_POST['leavesPerMonth'];
        $admin = new Administrator($empID);
        generateNumberOfLeavesPerMonthForm(Administrator::getLeavesPerMonth());
    }
}

else
    echo "Error: Not authorized!!";

?>
