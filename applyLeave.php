<?php
	if($_POST["fromDate"] != "" && $_POST["toDate"] != "" && $_POST["type"] != ""){
		include_once "includes/Employee.php";
		$today = new DateTime();
		session_start();
		$emp = new Employee($_SESSION["empID"]);
        if(date_create($_POST["toDate"]) < date_create($_POST["fromDate"])) {
            echo "Error: \"From date\" must be before \"To date\".";
            exit;
        }
        else if(date_create($_POST['fromDate']) < $today || date_create($_POST['toDate'] < $today)) {
        	echo "From date cannot be less that today";
        }
        else {
		    $leaveID = $emp->applyForLeave($_POST["fromDate"], $_POST["toDate"], $_POST["type"], $_POST["reason"]);
		    include_once "includes/Leave.php";
		    include_once "includes/Administrator.php";
		    $leave = new Leave($leaveID);
		    $leaveCount = $leave->getWorkingLeaveCount();
		    $availableLeaves = $emp->getAvailableLeaves();
		    $leavesPerMonth = Administrator::getLeavesPerMonth();
		    $toDate = $leave->getToDate();
		    $toMonth = date_create($toDate)->format('m');
		    $thisMonth = $today->format('m');
		    $availableLeaves = $availableLeaves + ($toMonth - $thisMonth)*$leavesPerMonth;
		    if($leaveCount>$availableLeaves) {
		    	$leave->delete();
		    	echo "Sorry, You only have $availableLeaves leaves left";
		    }
		    else echo "ok";
        }
	}
	else {
	    
	}
	
?>
