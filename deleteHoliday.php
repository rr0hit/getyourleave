<?
session_start();

if($_SESSION['type'] == 'administrator') {
    require_once("includes/Administrator.php");
    function delete($id) {
        $admin = new Administrator($_SESSION['empID']);
        $admin->deleteHoliday($id);
        header("location: viewHolidays.php");
    }

    if (isset($_GET["hid"])) 
        delete($_GET["hid"]);

    else if (isset($_POST["hid"]))
        delete($_POST["hid"]);

    else {
        require_once("generateHtml.php");
        $admin = new Administrator($_SESSION['empID']);
        $holidays = $admin->getHolidays();
        viewHolidays($holidays, true);
        viewDeleteHoliday();
    }
}

else
    echo "You are not authorized to view this page!!";

?>
