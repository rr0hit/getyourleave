<!DOCTYPE html>

<html>
    <head>
        <link href="css/bootstrap.min.css" media="all" type="text/css" rel="stylesheet">
        <script src="js/jquery-1.10.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/login.js"></script>
        <style>
            .container {
                padding-top: 150px;
            }
        </style>
    </head>

    <title>
        Login
    </title>

<body>
  <div class="container">
    <div class="row">
        <div class="span4 offset4 well">
            <legend>Login</legend>
                <div class="alert alert-error">
                    <a class="close" href="#" data-dismiss="alert">x</a>
                    Incorrect Username or Password!
                </div>
                <form method ="post" action ="login.php" >
                    <input type="text" name="email" class="span4" id="email" placeholder="Email"/>
                    <input type="password" name="password" class="span4" id="password" placeholder="Password"/>
                    <button type="submit" name="submit" class="btn btn-block">Sign In</button>
                </form>
        </div>
    </div>
  </div>

</body>

</html>
