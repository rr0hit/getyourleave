<?
session_start();

if($_SESSION['type'] == 'administrator') {
    require_once "generateHtml.php";
    require_once "includes/Administrator.php";
    session_start();
    $admin = new Administrator($_SESSION['empID']);
    $empArray = Array();
    if(isset($_GET['search']))
        $empArray = $admin->getEmployeesByName($_GET['search'] . '%');
    else
        $empArray = $admin->getEmployees();
    viewEmployees($empArray, true, false);
}
?>
