<?
session_start();

if($_SESSION['type'] == 'administrator') {
    require_once "generateHtml.php";
    require_once "includes/Administrator.php";
    session_start();
    $admin = new Administrator($_SESSION['empID']);
    $remArray = $admin->getReminderPolicies();
    viewReminderPolicies($remArray);
}
?>
