var setmode = false;
var selectedEmp = 0;

function loadToMainContent(page, choice, data) {
   $('.active').removeClass('active');
   $("#mainContent").load(page, data, function (){
        $('.details').click(function (){
            $('#popuptitle').html("Leave Details");
            loadToPopUp($(this).attr('href'), $(this).parent());
            $('#savebtn').hide();
        });

        $('.statusbtn').click(function () {
            $('#popuptitle').html( $(this).attr('action') +" Leave Application");
            loadToPopUp($(this).attr('href'), $(this).parent());
        });

        $('.delbtn').click(function () {
            var thisbtn = $(this);
            $.post($(this).attr('href'), {}, function (data) {
                $('#'+thisbtn.attr('after-del')).trigger('click');            
            });
        });
        
        $('#namesearchform').submit(function (e) {
            e.preventDefault();
        });

        $('#namesearchbtn').click(function () {
            if($('.manbtn').html() != $('.manbtn').attr('default'))
                setmode = true;
            loadToMainContent($('#namesearchform').attr('target'), $($(this).attr('after-search')).parent(), $("#namesearchform").serialize());       
        });

        if(setmode) {
            $('.manbtn').html($('.manbtn').attr('settext'));
            $('.manbtn').removeClass('btn-standard');
            $('.manbtn').addClass('btn-success');
            setmode = false;
        }
        else {
            $('.manbtn').html($('.manbtn').attr('default'));
            $('.manbtn').removeClass('btn-success');
            $('.manbtn').addClass('btn-standard');
        }

        $('.manbtn').click(function () {
            if($('.manbtn').html() == $('.manbtn').attr('default')) {
                selectedEmp = $(this).attr('empID');
                $('.manbtn').html($('.manbtn').attr('settext'));
                $('.manbtn').removeClass('btn-standard');
                $('.manbtn').addClass('btn-success');
            }

            else {
                $.post("changeManager.php", {'empID': selectedEmp, 'manID': $(this).attr('empID')}, function(data) {
                    setmode = false;                
                    $('#viewEmployees').trigger('click');
                    if (data != "ok") {
                        $('.modal').modal('show');
                        $('#popup').html('');
                        $('.alert').html(data);
                        $('.alert').show();
                        $('#savebtn').hide();
                    } 
                });
            }
        });
    });
   choice.addClass('active');
}

function loadToPopUp(page, choice) {
    $('.alert').hide();
    $('.active').removeClass('active');
    $('.modal').modal('show');
    $('#popup').load(page, function() {
        
        choice.addClass('active');
    });    
}

$('document').ready(function() { 
    $('.popover').hide();
    $('.alert').hide();

    $('.modal').on('hidden', function () {
        $('#savebtn').show();
    })

    $('#viewTeam').click(function(){
        loadToMainContent("viewManagedEmployees.php", $(this).parent());
        
    });

    $('#viewPendingRequests').click(function(){
        loadToMainContent("viewPendingRequests.php", $(this).parent());
    });

    $('#viewAcceptedRequests').click(function(){
        loadToMainContent("viewAcceptedRequests.php", $(this).parent());
    });

    $('#viewRejectedRequests').click(function(){
        loadToMainContent("viewRejectedRequests.php", $(this).parent());
    });

    $('#viewAppliedLeaves').click(function(){
        loadToMainContent("viewAppliedLeaves.php", $(this).parent());
    });

    $('#viewAppliedLeaves').trigger("click");

    $('#viewEmployees').click(function(){
        loadToMainContent("viewEmployees.php", $(this).parent());
    });

    $('#viewHolidays').click(function(){
        loadToMainContent("viewHolidays.php", $(this).parent());
    });

    $('#viewReminderPolicy').click(function(){
        loadToMainContent("viewReminderPolicies.php", $(this).parent());
    });

    $('#applyForLeave').click(function(){
        $('#popuptitle').html("Apply For Leave");
        loadToPopUp("applyForLeave.php", $(this).parent());
    });

    $('#addEmployee').click(function(){
        $('#popuptitle').html("Add Employee");
        loadToPopUp("addEmployee.php", $(this).parent());
    });

    $('#addHolidayList').click(function(){
        $('#popuptitle').html("Add Holidays");
        loadToPopUp("uploadHolidaysCSV.php", $(this).parent());
        $('#savebtn').hide();
    });

    $('#addAHoliday').click(function(){
        $('#popuptitle').html("Add Holidays");
        loadToPopUp("addHoliday.php", $(this).parent());
    });

    $('#addReminderPolicy').click(function(){
        $('#popuptitle').html("Add Reminder Policy");
        loadToPopUp("addReminderPolicy.php", $(this).parent());
    });

    $('#noOfLeavesPerMonth').click(function(){
        $('#popuptitle').html("Set leaves per month");
        loadToPopUp("numberOfLeavesPerMonth.php", $(this).parent());
    });

    $('#savebtn').click(function () {
        $.post($('#popup').find('form').attr('action'), $("#popup").children().serialize(), function (data) {            
            if(data == "ok") {
                $('.modal').modal('hide');
                $('#' + $('#popup').find('form').attr('after-add')).trigger('click');
            }

            else {
                $('.alert').show();
                $('.alert').html(data + "<a class='closealert close' href='#'>&times;</a>");
                $('.closealert').click( function () {
                    $('.alert').hide();        
                });
            }
        });
    });

});
