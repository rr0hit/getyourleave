<!DOCTYPE html>
<html>
<head>
    <link href="css/bootstrap.min.css" media="all" type="text/css" rel="stylesheet">
    <link href="css/jquery-ui.css" media="all" type="text/css" rel="stylesheet">
    <style>
        .table th, .table td {
            text-align: center;
            vertical-align: middle;
        }
    </style>    
    <script src="js/jquery-1.10.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src = "js/jquery-ui.js"></script>
    <script src="js/dashboard.js"></script>    
</head>
<title>Dashboard</title>
	<body>
		<?php 
			require_once "generateHtml.php";
			session_start();
            generateHeader();
			generateMenu($_SESSION["type"]);
		?>
    <div id='mainContent' class="well span12">
    </div>
    <div class="modal hide fade">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 id="popuptitle">Modal header</h3>
    </div>
    <div class="alert alert-error" style="border-radius: 0px">
    error
    <a class="closealert close" href="#">&times;</a>
    </div>
    <div id="popup" class="modal-body">
    </div>
    <div class="modal-footer">
    <a href="#" class="btn closebtn" data-dismiss="modal">Close</a>
    <a href="#" class="btn btn-primary" id="savebtn">Save changes</a>
    </div>
    </div>
    <div class='popover fade right in' style='top: 5.5px; left: 414.5px; display: block;'>
          <div class='arrow'></div>
          <h3 class='popover-title'>Instructions</h3>
          <div class='popover-content'>[APPLICANT] for applicant name. </br></br>
          [MANAGER] for manager name.</br></br>
          [FROM] for leave start date.</br></br>
          [TO] for leave end date.</br></br>
          [TYPE] for leave type.</br></br>                        
          </div>
    </div> 
	</body>
</html>
