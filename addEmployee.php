<?
session_start();
if($_SESSION['type'] == 'administrator') {

    if (isset($_POST['name'])) {
        require_once("includes/Administrator.php");
        $empID = $_SESSION["empID"];
        $name = $_POST["name"];
        $email = $_POST["email"];
        if($_POST['manID'] != "") $manID = $_POST["manID"];
        $admin = new Administrator($empID);
        if($_POST['manID'] != "") $admin->addEmployee($name, $email, $manID);
        else $admin->addEmployee($name, $email, $empID);
        echo "ok";
    }

    else {
        require_once("generateHtml.php");
        viewAddEmployee(); 
    }

}

else
    echo "You are not authorized to view this page!!";

?>
