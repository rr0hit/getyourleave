<?
require_once("includes/Leave.php");
session_start();
$leave = new Leave($_GET[lid]);
if($_SESSION['empID'] == $leave->getApplicantID()) {
    $leave->delete();
}
else {
    echo "You are not authorized to perform this action.";
}
?>
