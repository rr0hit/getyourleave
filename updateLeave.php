<?

if($_POST["fromDate"] != "" && $_POST["toDate"] != "" && $_POST["type"] != ""){
  include_once "includes/Leave.php";
  session_start();
  $leaveID = $_POST["leaveID"];
  $leave = new Leave($leaveID);
  $leave->setFromDate($_POST["fromDate"]);
  $leave->setToDate($_POST["toDate"]);
  $leave->setType($_POST["type"]);
  $leave->setReason($_POST["reason"]);
  header("location: viewLeaveDetails.php?lid=$leaveID");
  exit;
}

else {
  header("location: applyForLeave.php");
  exit;
}

?>
