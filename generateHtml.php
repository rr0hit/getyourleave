<?php

function generateMenu($type){

	include_once "includes/Employee.php";
	$emp = new Employee($_SESSION["empID"]);
	$name = $emp->getName();
	$employeeID = $emp->getEmployeeID();

	
	echo "<div class='span3 well' style='max-width: 200px; padding: 8px 0;'>
				<ul id='employeeMenu' class='nav nav-list'>";

	if($type != 'administrator') {
		echo "
	    <li><a href='#' id='viewAppliedLeaves'>View applied leaves</a></li>
		<li><a href='#' id='applyForLeave'>Apply for leave</a></li>";
	}

	if($type == "manager" || $type == "administrator"){
		echo "
         <li class='divider'></li>
		<li><a href='#' id='viewTeam'>View Team</a></li>
		<li><a href='#' id='viewPendingRequests'>View pending requests</a></li>
		<li><a href='#' id='viewAcceptedRequests'>View accepted requests</a></li>
		<li><a href='#' id='viewRejectedRequests'>View rejected requests</a></li>";
	}

	if($type == "administrator"){
		echo "
         <li class='divider'></li>
        <li><a href='#' id='viewEmployees'>View Employee list</a></li>
		<li><a href='#' id='addEmployee'>Add a new employee</a></li>
        <li><a href='#' id='viewHolidays'>View holiday list</a></li>
		<li><a href='#' id='addHolidayList'>Upload holiday list</a></li>
		<li><a href='#' id='addAHoliday'>Add a Holiday</a></li>
        <li><a href='#' id='viewReminderPolicy'>View remainder policies</a></li>
		<li><a href='#' id='addReminderPolicy'>Add Remainder Policy</a></li>
		<li><a href='#' id='noOfLeavesPerMonth'>Set leaves per month</a></li>";
	}

	echo "<li class='divider'></li><li><a href='index.php' id='logout'>logout</a></li></ul></div>";
}

function generateLeavesTable($leaveArray, $isManager){

	echo "<table class='table table-striped table-bordered'><tr>";
	if($isManager) echo "<th class='span2'>Employee</th>";
	else echo	"<th class='span4'>Manager</th>";
	echo "<th>From Date</th><th>To Date</th><th class='span1'>Working days</th><th>Status</th>";
	if($isManager) echo "<th>Take Action</th>";
	echo	"<th></th></tr>";

	for($i=0;$i<sizeof($leaveArray);$i++){
	echo "<tr>";
	if($isManager){
		$emp = new Employee($leaveArray[$i]->getApplicantID());
		$cellValue = $emp->getName();
	}
	else {
		$man = new Employee($leaveArray[$i]->getManagerID());
		$cellValue = $man->getName();
	}
	echo "<td>$cellValue</td>";
	$from = $leaveArray[$i]->getFromDate();
	echo "<td>$from</td>";
	$to = $leaveArray[$i]->getToDate();
	echo "<td>$to</td>";
	$numberOfDays = $leaveArray[$i]->getWorkingLeaveCount();
	echo "<td>$numberOfDays</td>";
	$status = $leaveArray[$i]->getStatus();
	echo "<td>$status</td>";
	$leaveID = $leaveArray[$i]->getLeaveID();
	if($isManager) {
		echo "<td>";
		if($status!="accepted") echo "<button action='Accept' class='statusbtn btn btn-success' style='margin: 0px 10px' href='acceptLeave.php?lid=$leaveID'>accept</button>";
		if($status!="rejected") echo "<button action='Reject' class='statusbtn btn btn-danger' style='margin: 0px 10px' href='rejectLeave.php?lid=$leaveID'>reject</button>";
		echo "</td>";
	}
	if($isManager)	echo "<td><button class='details btn btn-info' href='viewLeaveDetailsManager.php?lid=$leaveID'>View details</button></td>";
	else {
        echo "<td><button class='details btn btn-info' style = 'margin: 0px 10px' href='viewLeaveDetailsEmployee.php?lid=$leaveID'>Details</button>";
        echo "<button class='delbtn btn btn-danger' after-del='viewAppliedLeaves' style = 'margin: 0px 10px' href='deleteLeave.php?lid=$leaveID'>Cancel</button></td>";
    }
	echo "</tr>";
	}
	echo "</table>";
}

function generateRemarksForm($leaveID, $isAccepted){
    $afterAdd = "viewRejectedRequests";    
    if($isAccepted)
        $afterAdd = "viewAcceptedRequests";
	echo "
		<form class='span4' action='addRemarks.php' method='post' after-add='$afterAdd'>
			<textarea class = 'span6' name='remarks' id='remarks' rows='10' cols='70' placeholder='remarks'></textarea>
		";
        echo "<input type='hidden' name='lid' value=$leaveID />";
		if($isAccepted) echo "<input type='hidden' name='status' value='accept'/>";
		else echo "<input type='hidden' name='status' value='reject'/>";
		echo "</form>";
}

function generateViewLeaveDetailsEmployee($leaveID){

	include_once "includes/Leave.php";
	include_once "includes/Employee.php";
	$leave = new Leave($leaveID);
	$from = $leave->getFromDate();
	$to = $leave->getToDate();
	$numberOfDays = $leave->getWorkingLeaveCount();
	$emp = new Employee($leave->getApplicantID());
	$man = new Employee($leave->getManagerID());
	$name = $man->getName();
	$type = $leave->getType();
	$reason = $leave->getReason();
	$status = $leave->getStatus();
	$remarks = $leave->getRemarks();
	$leavesTillNow = $emp->getLeavesCount();
	$appliedOn = $leave->getAppliedOn();
	$leaveID = $leave->getLeaveID();
	
	echo "
    <div class='span4 offset1 well'>
    <table>
		<tr><td class='span2'><strong>From</strong></td><td class='span2'>$from</td></tr>
		<tr><td><strong>To</strong></td><td>$to</td></tr>
		<tr><td><strong>Type</strong></td><td>$type</td></tr>
		<tr><td><strong>Reason</strong></td><td>$reason</td></tr>
		<tr><td><strong>Manager</strong></td><td>$name</td></tr>
		<tr><td><strong>Status</strong></td><td>$status</td></tr>
		<tr><td><strong>Remarks</strong></td><td>$remarks</td></tr>
		<tr><td><strong>Applied On</strong></td><td>$appliedOn</td></tr>
		<tr><td><strong>Leaves taken</strong></td><td>$leavesTillNow</td></tr></table>";
	//echo "<tr><a href='editLeave.php?lid=$leaveID'>Edit leave details</a></tr></table>";
	}

function generateViewLeaveDetailsManager($leaveID){
	include_once "includes/Leave.php";
	include_once "includes/Employee.php";
	$leave = new Leave($leaveID);
	$from = $leave->getFromDate();
	$to = $leave->getToDate();
	$numberOfDays = $leave->getWorkingLeaveCount();
	$emp = new Employee($leave->getApplicantID());
	$name = $emp->getName();
	$type = $leave->getType();
	$reason = $leave->getReason();
	$status = $leave->getStatus();
	$remarks = $leave->getRemarks();
	$leavesTillNow = $emp->getLeavesCount();
	$appliedOn = $leave->getAppliedOn();
	$leaveID = $leave->getLeaveID();
	echo "
        <div class='span4 offset1 well'>
        <table>
        <tr><td class='span2'><strong>Employee</strong></td><td class='span2'>$name</td></tr>
		<tr><td class='span2'><strong>From</strong></td><td class='span2'>$from</td></tr>
		<tr><td><strong>To</strong></td><td>$to</td></tr>
		<tr><td><strong>Type</strong></td><td>$type</td></tr>
		<tr><td><strong>Reason</strong></td><td>$reason</td></tr>
		<tr><td><strong>Status</strong></td><td>$status</td></tr>
		<tr><td><strong>Remarks</strong></td><td>$remarks</td></tr>
		<tr><td><strong>Applied On</strong></td><td>$appliedOn</td></tr>
		<tr><td><strong>Leaves taken</strong></td><td>$leavesTillNow</td></tr></table>";
}

function generateApplyForLeave(){
	echo "
			<script src='js/datePicker.js'></script>
			<form method='post' action='applyLeave.php' after-add='viewAppliedLeaves' class='form-horizontal'>
                    <div class='control-group'>
					<label class='control-label'>From date</label>
                    <div class='controls'>                    
                    <input type='text' name='fromDate' class='span4 datepicker' id='fromDate' />
                    </div>
                    </div>
                    <div class='control-group'>
					<label class='control-label'>To date</label>
                    <div class='controls'>                    
                    <input type='text' name='toDate' class='span4 datepicker' id='toDate' />
                    </div>
                    </div>
                    <div class='control-group'>
					<label class='control-label'>Type</label> 
                    <div class='controls'>                    
                    <select type='text' name = 'type' class='span4' id='type'>
							<option>sick</option>
							<option>planned</option>
							<option>work from home</option>
					</select>
                    </div>
                    </div>
                    <div class='control-group'>
					<label class='control-label'>Reason</label> 
                    <div class='controls'>                    
                    <textarea type='text' name='reason' class='span4' id='reason' />
                    </div>
                    </div>
			</form>
	";

}

function viewAppliedLeaves($empID){
	
	include_once "includes/Employee.php";
	$emp = new Employee($empID);
	$currentLeaves = $emp->getCurrentLeaves();
	

	echo "
		<div id='activeLeaves'>
			<ul>
	";
	generateLeavesTable($currentLeaves, false);
	echo "
				<li><a href='dashboard.php' id='backToDashboard'>Back to Dashboard</a></li>
			</ul>
		</div>
	";
}

function viewPendingRequests($empID){
	include_once "includes/Manager.php";
	$man = new Manager($empID);
	$leaveRequests = $man->getPendingApplications();

	echo "
		<div id='viewLeaveRequests'>
		<ul>
	";
	generateLeavesTable($leaveRequests, true);
	echo"
				<li><a href='dashboard.php' id='backToDashboard'>Back to Dashboard</a></li>
			</ul>
		</div>
	";
}
function viewAcceptedRequests($empID){
	include_once "includes/Manager.php";
	$man = new Manager($empID);
	$leaveRequests = $man->getAcceptedApplications();

	echo "
		<div id='viewLeaveRequests'>
		<ul>
	";
	generateLeavesTable($leaveRequests, true);
	echo"
				<li><a href='dashboard.php' id='backToDashboard'>Back to Dashboard</a></li>
			</ul>
		</div>
	";
}

function viewRejectedRequests($empID){
	include_once "includes/Manager.php";
	$man = new Manager($empID);
	$leaveRequests = $man->getRejectedApplications();

	echo "
		<div id='viewLeaveRequests'>
		<ul>
	";
	generateLeavesTable($leaveRequests, true);
	echo"
				<li><a href='dashboard.php' id='backToDashboard'>Back to Dashboard</a></li>
			</ul>
		</div>
	";
}

function generateEditLeave($leaveID){
    $leave = new Leave($leaveID);
    $from = $leave->getFromDate();
    $to = $leave->getToDate();
    $reason = $leave->getReason();

	echo "
		<div id='editLeave'>
			<form method='post' action='updateLeave.php'>
				<ul>
					<li>From date<input type='date(YYYY-MM-DD)' name='fromDate' class='dateSelect' id='fromDate' value='$from'/></li>
					<li>To date<input type='date(YYYY-MM-DD)' name='toDate' class='dateSelect' id='toDate' value='$to'/></li>
					<li> Type <select type='text' name = 'type' class='dropDownSelect' id='type'>
							<option>sick</option>
							<option>planned</option>
							<option>work from home</option>
						</select></li>
					<li>Reason <input type='text' name='reason' class='box' id='reason' value='$reason'/></li>
                    <li> <input type='hidden' name = 'leaveID' value = '$leaveID'/> </li>
					<li><input type='submit' name='applyLeave' value='Apply' class='button' id='applyleave' /></li>
					<li><a href='dashboard.php' id='backToDashboard'>Back to Dashboard</a></li>
				</ul>
				</form>
		</div>
	";

}

function viewEmployees($empArray, $isAdmin, $whileDeleting) {
    if($isAdmin)
        generateSearchByName('viewEmployees.php', '#viewEmployee');
    else
        generateSearchByName('viewManagedEmployees.php', '#viewTeam');
	include_once "includes/Employee.php";
  echo "<table class='table table-striped table-bordered'>\n" . 
      "\t<tr>\n" .
      "\t\t<th> Name </th>\n" .
      "\t\t<th> Email </th>\n" .
      "\t\t<th> Type </th>\n".
      "\t\t<th> Joining Date </th>\n" .
      "\t\t<th> Leaves Taken </th>\n" ;
  if($isAdmin) {
  	echo "\t\t<th>Manager</th>\n";
    echo "\t<th class='span3'></th>";
    echo "\t<th></th>";
  }
  
  echo "\t</tr>\n";

  foreach($empArray as $k => $v) {
    echo "\t<tr>\n".
      "\t\t<td class='span2'> ". $v->getName(). " </td>\n".
      "\t\t<td> ". $v->getEmail(). " </td>\n".
      "\t\t<td>". $v->getType()." </td>\n".
      "\t\t<td> " . $v->getJoiningDate(). " </td>\n".
      "\t\t<td> ". $v->getLeavesCount(). " </td>\n";

    if($isAdmin) {
    	$employeeID = $v->getEmployeeID();
    	$man = new Employee($v->getManager());
    	$manName = $man->getName();
    	echo "\t\t<td>".$manName."</td><td><button class='manbtn btn btn-primary' default='Change Manager' settext='Set as Manager' empID='$employeeID'></button>"."</td>\n";
    	if(!$whileDeleting) echo "\t\t<td><button class='delbtn btn btn-danger' after-del='viewEmployees' href='deleteEmployee.php?empID=$employeeID'>Delete</button></td>\n";
    }
    echo "\t</tr>\n";
  }
  echo "</table>";
  echo "<li><a href='dashboard.php' id='backToDashboard'>Back to Dashboard</a></li>";
}

function generateChangeManagerForm($empID){
	echo "
	<form action='changeManager.php?empID=$empID' method='post'>
		<ul>
			<li>Manager ID <input type='number' name='changeManager' id='changeManagerInput' /></li>
			<li><input type='submit' name='changeManagerSubmit' value='Change' class='button' id='changeManagerSubmit' /></li>
		</ul>
	</form>
	<li><a href='dashboard.php' id='backToDashboard'>Back to Dashboard</a></li>
	";
}

function viewAddEmployee() {

    echo "<form method='post' action='addEmployee.php' after-add='viewEmployees' class='form-horizontal'>
                <div class='control-group'>
                <label class='control-label'>Name</label>
                <div class='controls'>
                <input type='text' name='name' class='span4'/>
                </div>                
                </div>
                <div class='control-group'>
                <label class='control-label'>Email</label>
                <div class='controls'> 
                <input type='text' name='email' class='span4'/>
                </div>
                </div>
          </form>";
}

function viewDeleteEmployee() {
    echo "<form method='post' action='deleteEmployee.php'>
            <ul>
                <li>ID of Employee to delete <input type='text' name='empID'/></li>
                <li><input type='submit' value='Delete'/></li>
            </ul>
          </form>";
}

function viewUploadCSV() {
    echo "<form enctype='multipart/form-data' action='uploadHolidaysCSV.php' method='post' after-add='viewHolidays' class='form-horizontal'>
                <div class='control-group'>
                <label class='control-label'> Choose the csv file</label> 
                <div class='controls'>
                <input type='file' name='csv_file'/>
                <input type='submit' value='Submit'/>
                </div>
                </div>
        	</form>";
}

function viewHolidays($holidayArray, $whileDeleting) {
    echo "<table class='table table-striped table-bordered'>
            <tr> <th>ID</th><th>Date</th><th>Holiday</th></tr>";
    foreach($holidayArray as $k=>$v) {
        $d = $v->getDate();
        $n = $v->getName();
        $i = $v->getHolidayID();
        echo "<tr>
        			<td>$i</td>
              <td>$d</td>
              <td>$n</td>";
        if(!$whileDeleting) echo  "<td> <button class='delbtn btn btn-danger' after-del='viewHolidays' href='deleteHoliday.php?hid=$i'>delete</a></td>";
        echo  "</tr>";
    }
    echo "</table>";
    echo "<li><a href='dashboard.php' id='backToDashboard'>Back to Dashboard</a></li>";
}

function generateAddHolidayForm(){
    echo "<script src='js/datePicker.js'></script>";
	echo "
		<form method='post' action='addHoliday.php' after-add='viewHolidays' class='form-horizontal'>
                <div class='control-group'>
				<label class='control-label'>Holiday Date</label>
                <div class='controls'><input type='text' name='holidayDate' class='datepicker'/></div></div>
                <div class='control-group'>
				<label class='control-label'>Name of Holiday</label>
                <div class='controls'><input type='text' name='nameOfHoliday'/></div></div>
		</form>

	";
}

function viewDeleteHoliday() {
    echo "<form method='post' action='deleteHoliday.php'>
            <ul>
                <li>ID of Holiday to delete <input type='text' name='hid'/></li>
                <li><input type='submit' value='Delete'/></li>
            </ul>
          </form>";
}

function generateNumberOfLeavesPerMonthForm($currentLeaevsPerMonth){
	echo "
		<form method='post' action='numberOfLeavesPerMonth.php' class='form-horizontal'>
            <div class='control-group'>
			<label class='control-label'>Leaves per month:</label>
            <div class='controls'>
            <input value='$currentLeaevsPerMonth' type='number' name='leavesPerMonth' id='leavesPerMonth'/>
            </div>
            </div>
		</form>
	";
}

function viewReminderPolicies($reminderArray) {
    echo "<table class='table table-striped table-bordered'>
            <tr> 
                <th>ID</th>
                <th>Days Before</th>
                <th>Template</th>
                <th>CC List</th>
            </tr>";
    foreach($reminderArray as $k=>$v) {
        $d = $v->getDaysBefore();
        $n = $v->getTemplate();
        $i = $v->getReminderID();
        $c = $v->getCCList();
        echo "<tr>
        	  <td>$i</td>
              <td>$d</td>
              <td>$n</td>
              <td>$c</td>
              <td> <button class='delbtn btn btn-danger' after-del='viewReminderPolicy' href='deleteReminder.php?rid=$i'>delete</a></td>
              </tr>";
    }
    echo "</table>";
    echo "<li><a href='dashboard.php' id='backToDashboard'>Back to Dashboard</a></li>";
}

function viewAddReminderPolicy() {
    echo "<form method='post' action='addReminderPolicy.php' after-add='viewReminderPolicy' class='form-horizontal'>
                <div class='control-group'>
                <label class='control-label'>Days before</label> 
                <div class='controls'>                
                <input type='text' name='daysBefore'/>
                </div>
                </div>
                <div class='control-group'>
                <label class='control-label'>Template</label>
                <div class='controls'> 
                <textarea type='text' rows='5' name='template' id='templatein'></textarea>
                </div></div>
                <div class='control-group'>
                <label class='control-label'>CC List</label> 
                <div class='controls'>
                <textarea type='text' name='ccList' rows='5'></textarea>
                </div></div>
            </ul>
          </form>";
}

function generateHeader() {
    include_once "includes/Employee.php";
    $emp = new Employee($_SESSION["empID"]);
	$name = $emp->getName();
    echo "    <div class='navbar'>
    <div class='navbar-inner'>
    <a class='brand' href='#'>GetYourLeave</a>
    <div class='pull-right'>
    <h4> Welcome $name </h4>
    </div>
    </div>
    </div>";
}

function generateSearchByName($target, $afterSearch) {
    echo "<form id='namesearchform' class='form-search pull-right' target='$target' after-search='$afterSearch'>
    <input type='text' name='search'>
    <button id='namesearchbtn' class='btn'>Search</button>
    </form>";
}
?>
