<?
    session_start();
    if($_SESSION['type'] == 'administrator') {
        require_once("includes/Administrator.php");
        $admin = new Administrator($_SESSION['empID']);
        require_once("generateHtml.php");
        $holidays = $admin->getHolidays();
        viewHolidays($holidays, false);
    }

    else
        echo "You are not authorized to view this page!!";

?>
