<?
session_start();

if($_SESSION['type'] == 'administrator') {
    require_once("includes/Administrator.php");

    function delete($id) {
        $admin = new Administrator($_SESSION['empID']);
        $admin->deleteReminderPolicy($id);
        header("location: viewReminderPolicies.php");
    }

    if (isset($_GET["rid"])) 
        delete($_GET["rid"]);

    else if (isset($_POST["rid"]))
        delete($_POST["rid"]);
}

else
    echo "You are not authorized to view this page!!";

?>
