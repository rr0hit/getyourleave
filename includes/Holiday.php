<?
require_once("DBConnection.php");

class Holiday {
  private $holidayID;
  private $date;
  private $name;
  private $dbConn;

  function __construct($holidayID) {
    $this->holidayID = $holidayID;
    $this->dbConn = DBConnection::getDatabaseConnection();
    $res = $this->dbConn->query("SELECT date, name FROM holidays WHERE holidayID = $holidayID;");
    $row = $res->fetch_assoc();
    $this->date = $row["date"];
    $this->name = $row["name"];
  }

  function getDate() {
    return $this->date;
  }

  function getName() {
    return $this->name;
  }

  function getHolidayID() {
    return $this->holidayID;
  }

}

?>
