<?php
  
/* Read config file and open a connection to the DB. 
   Return the connection. 
*/

class DBConnection{
	private static $config;
	private static $link;

public static function init(){
	self::$config = json_decode(file_get_contents("/home/capillary/connectToDatabase.json"));
	self::$link = new mysqli(self::$config->host,self::$config->username,self::$config->password,self::$config->database);
}
public static function getDatabaseConnection(){
  return self::$link;
}
}

DBConnection::init();

?>
