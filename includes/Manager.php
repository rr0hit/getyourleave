<?php

require_once "Employee.php";

class Manager extends Employee{

  function __construct($empID){
    parent::__construct($empID);
  }

  public function approveLeave($leaveID, $remarks){
    $leave = new Leave($leaveID);
    if($leave->getManagerID() == $this->getEmployeeID())  $leave->approve($remarks);
  }

  public function rejectLeave($leaveID, $remarks){
    $leave = new Leave($leaveID);
    if($leave->getManagerID() == $this->empID)  $leave->reject($remarks);
  }

  public function getManagedUsers(){
    $result = $this->dbConn->query("SELECT empID FROM employees WHERE manID = $this->empID AND isActive = 1;");
    $row = $result->fetch_assoc();
    $empArray;
    while(!is_null($row)){
      $empArray[] = new Employee($row['empID']);
      $row = $result->fetch_assoc();
    }
    return $empArray;
  }

  public function getManagedUsersByName($search){
    $result = $this->dbConn->query("SELECT empID FROM employees WHERE manID = $this->empID AND isActive = 1 AND name LIKE '$search';");
    $row = $result->fetch_assoc();
    $empArray;
    while(!is_null($row)){
      $empArray[] = new Employee($row['empID']);
      $row = $result->fetch_assoc();
    }
    return $empArray;
  }

  public function getPendingApplications(){
    $result = $this->dbConn->query("SELECT leaveID FROM leaves WHERE manID = $this->empID AND fromDate >= now() AND status = 'pending' AND isActive  = 1;");
    return $this->getLeaveArray($result);
  }

  public function getAcceptedApplications(){
    $result = $this->dbConn->query("SELECT leaveID FROM leaves WHERE manID = $this->empID AND status = 'accepted' AND isActive = 1;");
    return $this->getLeaveArray($result);
  }

  public function getRejectedApplications(){
    $result = $this->dbConn->query("SELECT leaveID FROM leaves WHERE manID = $this->empID AND status = 'rejected' AND isActive = 1;");
    return $this->getLeaveArray($result);
  }

}

?>
