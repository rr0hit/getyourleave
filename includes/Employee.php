<?php

require_once "DBConnection.php";
require_once "Leave.php";
    
class Employee{
  protected $empID;
  protected $name;
  protected $email;
  protected $password;
  protected $type;
  protected $manID;
  protected $joiningDate;
  protected $dbConn;

  function __construct($empID){
    $this->empID = $empID;
    $this->dbConn = DBConnection::getDatabaseConnection();
    $res = $this->dbConn->query("SELECT * FROM employees WHERE empID = $empID;");
    $row = $res->fetch_assoc();
    $this->name = $row["name"];
    $this->email = $row["email"];
    $this->password = $row["password"];
    $this->type = $row["type"];
    $this->manID = $row["manID"];
    $this->joiningDate = $row["joiningDate"];
  }
  
  public function setName($name){
    $this->dbConn->query("UPDATE employees SET name = '$name' WHERE empID = $this->empID;");
    $this->name = $name;
  }

  public function setPassword($password){
    $this->dbConn->query("UPDATE employees SET password = '$password' WHERE empID = $this->empID;");
    $this->password = $password;
  }

  public function setEmail($email){
    $this->dbConn->query("UPDATE employees SET email = '$email' WHERE empID = $this->empID;");
    $this->email = $email;
  }

  public function setManager($manID) {
    $this->dbConn->query("UPDATE employees SET manID = $manID WHERE empID = $this->empID;");
    $this->manID = $manID;
  }

  public function setType($type) {
    $this->dbConn->query("UPDATE employees SET type = '$type' WHERE empID = $this->empID;");
    $this->type = $type;
  }

  public function getEmployeeID(){
    return $this->empID;
  }

  public function getName(){
    return $this->name;
  }

  public function getPassword(){
    return $this->password;
  }

  public function getEmail(){
    return $this->email;
  }

  public function getType(){
    return $this->type;
  }

  public function getManager(){
    return $this->manID;
  }

  public function getJoiningDate(){
    return $this->joiningDate;
  }

  // Apply for a new leave
  public function applyForLeave($fromDate, $toDate, $type, $reason) {
    $this->dbConn->query("INSERT INTO leaves (empID, manID, fromDate, toDate, type, reason, appliedOn) VALUES ($this->empID, $this->manID, '$fromDate', '$toDate', '$type', '$reason', now());");

    $res = $this->dbConn->query("SELECT leaveID FROM leaves WHERE empID = $this->empID AND fromDate = '$fromDate' AND toDate = '$toDate'");
    $ret = $res->fetch_assoc();
    return $ret['leaveID'];
  }

  // Cancel a leave application
  public function cancelLeave($leaveID){
    $this->dbConn->query("UPDATE leaves SET isActive = 0 WHERE leaveID = $leaveID;");
  }

  // View current leave applications.
  public function getCurrentLeaves(){
    $res = $this->dbConn->query("SELECT leaveID from leaves WHERE empID = $this->empID AND fromDate >= now() AND isActive = 1;");
    return $this->getLeaveArray($res);
  }

  // View former leave applications.
  public function getLeavesHistory(){
    $res = $this->dbConn->query("SELECT leaveID from leaves WHERE empID = $this->empID AND isActive = 1;");
    return $this->getLeaveArray($res);
  }

  public function getLeavesCount() {
    $res = $this->dbConn->query("SELECT leaveID from leaves WHERE empID = $this->empID AND fromDate < now() AND YEAR(fromDate) = YEAR(CURDATE()) AND status = 'accepted' AND isActive = 1");
    $count = 0;
    while($row = $res->fetch_assoc()) {
      $leave = new Leave($row['leaveID']);
      $count = $count + $leave->getWorkingLeaveCount();
    }
    return $count;
  }

  protected function getLeaveArray($res) {
    $ret = null;
    while($row = $res->fetch_assoc()) {
      $ret[] = new Leave($row['leaveID']);
    }
    return $ret;
  }

  public function getAvailableLeaves(){
    $leavesPerMonth = Administrator::getLeavesPerMonth();
    $today = new DateTime();
    $thisMonth = $today->format('m');
    return $thisMonth*$leavesPerMonth - $this->getLeavesCount();
  }

}

?>
