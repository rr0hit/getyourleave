<?php

require_once("DBConnection.php");

class Leave {
  private $leaveID;
  private $empID;
  private $manID;
  private $fromDate;
  private $toDate;
  private $type;
  private $reason;
  private $status;
  private $remarks;
  private $appliedOn;
  private $dbConn;
  private $isNotified;
  private $isActive;

  function __construct($leaveID) {
    $this->dbConn = DBConnection::getDatabaseConnection();
    $res = $this->dbConn->query("SELECT * from leaves where leaveID = $leaveID;");
    $row = $res->fetch_assoc();
    $this->leaveID = $row['leaveID'];
    $this->empID = $row['empID'];
    $this->manID = $row['manID'];
    $this->fromDate = $row['fromDate'];
    $this->toDate = $row['toDate'];
    $this->type = $row['type'];
    $this->reason = $row['reason'];
    $this->status = $row['status'];
    $this->remarks = $row['remarks'];
    $this->appliedOn = $row['appliedOn'];
    $this->isNotified = $row['isNotified'];
    $this->isActive = $row['isActive'];
  }

  public function delete() {
    $this->fromDate = $fromDate;
    $this->dbConn->query("UPDATE leaves SET isActive = 0 WHERE leaveID = $this->leaveID;");
  }

  public function setFromDate($fromDate) {
    $this->fromDate = $fromDate;
    $this->dbConn->query("UPDATE leaves SET fromDate = '$fromDate' WHERE leaveID = $this->leaveID;");
  }

  public function setToDate($toDate) {
    $this->toDate = $toDate;
    $this->dbConn->query("UPDATE leaves SET toDate = '$toDate' WHERE leaveID = $this->leaveID;");
  }
  
  public function setType($type) {
    $this->type = $type;
    $this->dbConn->query("UPDATE leaves SET type = '$type' WHERE leaveID = $this->leaveID;");
  }

  public function setReason($reason) {
    $this->reason = $reason;
    $this->dbConn->query("UPDATE leaves SET reason = '$reason' WHERE leaveID = $this->leaveID;");
  }

  public function resetNotification(){
    $this->dbConn->query("UPDATE leaves isNotified = 'false' WHERE leaveID = $this->leaveID");
  }

  public function approve($remarks) {
    $this->status = "accepted";
    $this->dbConn->query("UPDATE leaves SET status = '$this->status', remarks = '$remarks' WHERE leaveID = $this->leaveID;");
    $this->resetNotification();
  }

  public function reject($remarks) {
    $this->status = "rejected";
    $this->dbConn->query("UPDATE leaves SET status = '$this->status', remarks = '$remarks' WHERE leaveID = $this->leaveID;");
    $this->resetNotification();
  }

  public function setRemarks($remarks) {
    $this->remarks = $remarks;
    $this->dbConn->query("UPDATE leaves SET remarks = '$remarks' WHERE leaveID = $this->leaveID;");
  }

  public function getFromDate() { 
    return $this->fromDate;
  }

  public function getToDate() {
    return $this->toDate;
  }

  public function getType() {
    return $this->type;
  }

  public function getReason() {
    return $this->reason;
  }

  public function getStatus() {
    return $this->status;
  }

  public function getRemarks() {
    return $this->remarks;
  }

  public function getApplicantID() {
    return $this->empID;
  }

  public function getManagerID() {
    return $this->manID;
  }

  public function getLeaveID(){
    return $this->leaveID;
  }

  public function getAppliedOn(){
    $res = $this->dbConn->query("SELECT appliedOn FROM leaves WHERE leaveID = $this->leaveID");
    $row = $res->fetch_assoc();
    return $row["appliedOn"];
  }

  public function getWorkingLeaveCount(){

    $res = $this->dbConn->query("SELECT COUNT(hols.date) 
				                          AS holidaysCount FROM leaves 
				                          LEFT JOIN holidays AS hols 
				                          ON hols.date >= leaves.fromDate AND hols.date <= leaves.toDate 
				                          WHERE leaveID = $this->leaveID
                                  AND DAYOFWEEK(hols.date)<>'1' AND DAYOFWEEK(hols.date)<>'7'
                                ");
    $row = $res->fetch_assoc();
    $holidaysCount = $row['holidaysCount'];
    $res = $this->dbConn->query("SELECT 5 * ( DATEDIFF( toDate, fromDate ) DIV 7 ) + MID( '1234555512344445123333451222234511112345001234550',7 * WEEKDAY( fromDate ) + WEEKDAY( toDate ) +1, 1 ) 
            AS leavescount 
            FROM leaves
            WHERE leaveID = $this->leaveID
            ");
    $row = $res->fetch_assoc();
    $leavesCount = $row['leavescount'];
    return $leavesCount - $holidaysCount;
  }
}

?>
