<?php

require_once("Manager.php");
require_once("Holiday.php");
require_once("ReminderPolicy.php");

class Administrator extends Manager {
  
  private static $leavesPerMonth;
  private $holidays;
  private $employees;

  function __construct($empID) {
    parent::__construct($empID);
    $this->getLeavesPerMonth();
    $this->getHolidays();
  }

  static function getLeavesPerMonth() {
    $dbConn = DBConnection::getDatabaseConnection();
    $res = $dbConn->query("SELECT leavesPerMonth FROM config;");
    $row = $res->fetch_assoc();
    $leavesPerMonth = $row["leavesPerMonth"];
    return $leavesPerMonth;
  }

  function setLeavesPerMonth($leavesPerMonth) {
    $this->leavesPerMonth = $leavesPerMonth;
    $this->dbConn->query("UPDATE config SET leavesPerMonth = $leavesPerMonth;");
  }

  function addEmployee($name, $email, $manID) {
    $password = $this->generateRandomString();
    $type = "employee";
    $this->dbConn->query("INSERT INTO employees (name, email, password, type, manID, joiningDate) VALUES ('$name', '$email', '$password', '$type', $manID, now());");
    $manager = new Employee($manID);
    if ($manager->getType() != "administrator") {
      $manager->setType("manager");
    }
    $res = $this->dbConn->query("SELECT LAST_INSERT_ID() as lid;");
    $row = $res->fetch_assoc();
    $empID = $row["lid"];
    return $empID;
    }

  function deleteEmployee($empID) {
    $this->dbConn->query("UPDATE employees SET isActive = 0 WHERE empID = $empID");
    $manager = new Manager($empID);
    $emparray = $manager->getManagedUsers();
    foreach ($emparray as $k=>$v) {
        $v->setManager($manager->getManager());
    }
    $managersManager = new Manager($manager->getManager());
    if (count($managersManager->getManagedUsers()) == 0)
        $managersManager->setType("employee");
  }

  function isEmployee($empID){
    $res = $this->dbConn->query("SELECT count(*) as cnt FROM employees WHERE empID = $empID and isActive = '1'");
    $row = $res->fetch_assoc();
    if($row["cnt"] == 1) return true;
    else return false;
  }

  function setManager($empID, $manID) { 
    $employee = new Employee($empID);
    $oldManager = new Manager($employee->getManager());
    $employee->setManager($manID);
    $manager = new Manager($manID);
    if ($manager->getType() != "administrator") {
      $manager->setType("manager");
    }
    if(count($oldManager->getManagedUsers()) == 0) {
      $oldManager->setType("employee");
    } 
  }

  function addHolidayList($csvFile) {
    $csvFileResource = fopen($csvFile, 'r');
    $sql = "INSERT INTO holidays (date,name) VALUES";
    while($csvArray = fgetcsv($csvFileResource)) {
      $date = $csvArray[0];
      $name = addslashes($csvArray[1]);
      $sql = $sql."('$date', '$name'),";
    }
    $sql = substr($sql, 0, strlen($sql) - 1);
    $this->dbConn->query($sql);
    fclose($csvFileResource);
  }

  function getEmployees(){
    $res = $this->dbConn->query("SELECT empID FROM employees WHERE isActive='1';");
    $ret =null;
    while($row = $res->fetch_assoc()){
      $ret[] = new Employee($row["empID"]);
    }
    $this->employees = $ret;
    return $ret;
  }

  function getEmployeesByName($search){
    $res = $this->dbConn->query("SELECT empID FROM employees WHERE isActive='1' AND name LIKE '$search';");
    $ret =null;
    while($row = $res->fetch_assoc()){
      $ret[] = new Employee($row["empID"]);
    }
    $this->employees = $ret;
    return $ret;
  }

  function getHolidays() {
    $res = $this->dbConn->query("SELECT holidayID FROM holidays;");
    $ret = Array();
    while($row = $res->fetch_assoc()) {
      $ret[] = new Holiday($row["holidayID"]);
    }
    $this->holidays = $ret;
    return $this->holidays;
  }

  function deleteHolidayList($year) {
    $this->dbConn->query("DELETE FROM holidays WHERE date >= '$year-01-01' AND date <= '$year-12-31';");
    $this->getHolidays();
  }

  function addAHoliday($date, $name){
    $this->dbConn->query("INSERT INTO holidays (date, name) VALUES('$date', '$name');");
  }

  function deleteHoliday($holidayID) {
    $this->dbConn->query("DELETE FROM holidays WHERE holidayID = $holidayID;");
    $this->getHolidays();
  }

  function addReminderPolicy($daysBefore, $template, $ccList) {
    $this->dbConn->query("INSERT INTO reminders (daysBefore, template, ccList) VALUES ($daysBefore, '$template', '$ccList');");
  }

  function deleteReminderPolicy($reminderID) {
    $this->dbConn->query("DELETE FROM reminders WHERE id = $reminderID;");
  }

  function getReminderPolicies() {
    $res = $this->dbConn->query("SELECT id FROM reminders;");
    $ret = Array();
    while($row = $res->fetch_assoc()) {
      $ret[] = new ReminderPolicy($row["id"]);
    }
    return $ret;
  }

  function setNewLeaveTemplate($template) {
    $this->dbConn->query("UPDATE config SET newLeaveTemplate = '$template';");
  }

  function setLeaveUpdateTemplate($template) {
    $this->dbConn->query("UPDATE config SET updateLeaveTemplate = '$template';");
  }

  function getLeaveUpdateTemplate() {
    $res = $this->dbConn->query("SELECT updateLeaveTemplate FROM config;");
    $row = $res->fetch_assoc();
    return $row["updateLeaveTemplate"];
  }

  function getNewLeaveTemplate() {
    $res = $this->dbConn->query("SELECT newLeaveTemplate FROM config;");
    $row = $res->fetch_assoc();
    return $row["newLeaveTemplate"];
  }

  function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
  }

  function detectCycle($empID, $manID) {
    $isCycle = false;
    while($manID != $this->empID) {
        if ($manID == $empID) {
            $isCycle = true;
            break;
        }
        $employee = new Employee($manID);
        $manID = $employee->getManager();
    }

    return $isCycle;
  }
}

?>
