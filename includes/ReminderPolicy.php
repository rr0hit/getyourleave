<?
require_once("DBConnection.php");

class ReminderPolicy {
  private $reminderID;
  private $daysBefore;
  private $template;
  private $ccList;
  private $dbConn;

  function __construct($reminderID) {
    $this->reminderID = $reminderID;
    $this->dbConn = DBConnection::getDatabaseConnection();
    $res = $this->dbConn->query("SELECT daysBefore, template, ccList FROM reminders WHERE id = $reminderID;");
    $row = $res->fetch_assoc();
    $this->daysBefore = $row["daysBefore"];
    $this->template = $row["template"];
    $this->ccList = $row["ccList"];
  }

  function getDaysBefore() {
    return $this->daysBefore;
  }

  function getTemplate() {
    return $this->template;
  }

  function getCCList() {
    return $this->ccList;
  }

  function getReminderID() {
    return $this->reminderID;
  }

  function setTemplate($template) {
    $this->dbConn->query("UPDATE reminders SET template = '$template' WHERE id = $this->reminderID;");
  }

  function setDaysBefore($daysBefore) {
    $this->dbConn->query("UPDATE reminders SET daysBefore = $daysBefore WHERE id = $this->reminderID;");
  }

  function setCCList($ccList) {
    $this->dbConn->query("UPDATE reminders SET ccList = '$ccList' WHERE id = $this->reminderID;");
  }

}
