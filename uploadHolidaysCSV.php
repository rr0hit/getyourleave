<?
session_start();

if($_SESSION['type'] == 'administrator') {
    if(isset($_FILES["csv_file"])) {
        if($_FILES['csv_file']['type'] == 'text/csv')
        {
            $csvFile = $_FILES["csv_file"]["tmp_name"];
            $csv_file = fopen($csvFile, 'r');
            $isCsvFormat = true;
            while($row = fgetcsv($csv_file)){
                if(!preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $row[0])) {
                    $isCsvFormat = false;
                    break;
                }
            }
            if($isCsvFormat){
                require_once("includes/Administrator.php");
                $admin = new Administrator($_SESSION['empID']);
                $admin->addHolidayList($_FILES["csv_file"]["tmp_name"]);
                header("location: dashboard.php#");
            }
            else {
                echo "Date is not in the accepted format. It should be in YYYY-MM-DD format.<br>";
                echo "<a href='dashboard.php#' >dashboard</a>";
            }
        }
        else {
            echo "Invalid file format.<br>";
            echo "<a href='dashboard.php#' >dashboard</a>";
        }
        //echo "ok";
    }

    else {
        require_once("generateHtml.php");
        viewUploadCSV();
    }
}

else
    echo "You are not authorized to view this page!!";

?>