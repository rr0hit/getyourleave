<?
session_start();

if($_SESSION['type'] == 'administrator') {
    require_once("includes/Administrator.php");

    function delete($id) {
        $admin = new Administrator($_SESSION['empID']);
        $admin->deleteEmployee($id);
        header("location: viewEmployees.php");
    }

    if (isset($_GET["empID"])) 
        delete($_GET["empID"]);

    else if (isset($_POST["empID"]))
        delete($_POST["empID"]);

    else {
        require_once("generateHtml.php");
		$admin = new Administrator($_SESSION['empID']);
        $empArray = $admin->getEmployees();
				viewEmployees($empArray, true, true);
        viewDeleteEmployee();
    }
}

else
    echo "You are not authorized to view this page!!";

?>
