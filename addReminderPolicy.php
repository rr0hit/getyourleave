<?
session_start();
if($_SESSION['type'] == 'administrator') {

    if (isset($_POST['template'])) {
        require_once("includes/Administrator.php");
        $empID = $_SESSION["empID"];
        $template = $_POST["template"];
        $ccList = $_POST["ccList"];
        $daysBefore = $_POST["daysBefore"];
        $admin = new Administrator($empID);
        $admin->addReminderPolicy($daysBefore, $template, $ccList);
        echo "ok";
    }

    else {
        require_once("generateHtml.php");
        viewAddReminderPolicy(); 
    }

}

else
    echo "Error: Not Authorized!!";

?>
